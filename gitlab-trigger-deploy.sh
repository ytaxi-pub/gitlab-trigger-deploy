#!/bin/bash

# debug capabilities
if [ -n "$DEBUG" ]; then
  set -x
fi

# enable strict mode
set -euo pipefail

# configuration
: ${DEPLOY_ENV:=}
: ${DEPLOY_VARS:=}
: ${DEPLOY_TAGS:=}
: ${DEPLOY_PLAY:=}
: ${DEPLOY_INITIATOR:="${GITLAB_USER_NAME}"}
: ${DEPLOY_SOURCE_PIPELINE:="${CI_PIPELINE_URL}"}
: ${DEPLOY_SOURCE_JOB:="${CI_JOB_URL}"}

: ${GITLAB_SERVER_URL:="${CI_SERVER_URL}"}
: ${DEPLOYMENT_PROJECT_ID:=}
: ${DEPLOYMENT_BRANCH:="master"}
: ${DEPLOYMENT_PIPELINE_TRIGGER_TOKEN:=} # IaC->Settings->CI/CD->Pipeline triggers

: ${DEPLOYMENT_JUST_TRIGGER_AND_QUIT:=}
: ${DEPLOYMENT_PIPELINE_TIMEOUT_SECONDS:=300}
: ${DEPLOYMENT_PIPELINE_READ_API_TOKEN:=} # IaC->Settings->Access Tokens->read_api->Create
: ${DEPLOYMENT_PIPELINE_CHECK_INTERVAL_SECONDS:=10}

# action1: trigger pipeline

deployment_pipeline_trigger_url="${GITLAB_SERVER_URL}/api/v4/projects/${DEPLOYMENT_PROJECT_ID}/trigger/pipeline"

trigger_result=$(
  curl -sX POST -F token=${DEPLOYMENT_PIPELINE_TRIGGER_TOKEN} \
       -F ref=${DEPLOYMENT_BRANCH} \
       --form "variables[VARS]=${DEPLOY_VARS}" \
       --form "variables[ENV]=${DEPLOY_ENV}" \
       --form "variables[TAGS]=${DEPLOY_TAGS}" \
       --form "variables[PLAY]=${DEPLOY_PLAY}" \
       --form "variables[DEPLOY_INITIATOR]=${DEPLOY_INITIATOR}" \
       --form "variables[DEPLOY_SOURCE_PIPELINE]=${DEPLOY_SOURCE_PIPELINE}" \
       --form "variables[DEPLOY_SOURCE_JOB]=${DEPLOY_SOURCE_JOB}" \
       "${deployment_pipeline_trigger_url}"
	      )

deployment_pipeline_path=$(echo "${trigger_result}" | jq -r .detailed_status.details_path)
if [ "${deployment_pipeline_path}" = null ]; then
  echo "Trigger pipeline error. Please contact DevOps."
  echo "Trigger response:"
  echo "${trigger_result}" | jq .
  exit 1
fi

deployment_pipeline_user_url="${GITLAB_SERVER_URL}/${deployment_pipeline_path}"
echo "Deployment pipeline: ${deployment_pipeline_user_url}"


# action2: check and inherit the status of the deployment pipeline we triggered

if [ -n "${DEPLOYMENT_JUST_TRIGGER_AND_QUIT}" ]; then
  exit 0
fi

echo "Waiting deployment pipeline to finish. Timeout is ${DEPLOYMENT_PIPELINE_TIMEOUT_SECONDS} seconds."

deployment_pipeline_id=$(echo "${trigger_result}" | jq -r .id)
deployment_pipeline_url="${GITLAB_SERVER_URL}/api/v4/projects/${DEPLOYMENT_PROJECT_ID}/pipelines/${deployment_pipeline_id}"

function get_deployment_pipeline_status() {
  curl -s --header "PRIVATE-TOKEN: ${DEPLOYMENT_PIPELINE_READ_API_TOKEN}" "${deployment_pipeline_url}" | jq -r .status
}

timeout_time=$(( $(date +%s) + DEPLOYMENT_PIPELINE_TIMEOUT_SECONDS ))

while sleep "${DEPLOYMENT_PIPELINE_CHECK_INTERVAL_SECONDS}";
do
  if [ "$(date +%s)" -gt "$timeout_time" ]; then
    echo "Timeout. Contact DevOps."
    exit 2
  fi
  status=$(get_deployment_pipeline_status)
  echo "$(date +%T) Deployment status: $status"
  case "$status" in
  success)
    echo "Deployment SUCCESS"
    exit 0
    ;;
  failed|canceled|skipped)
    echo "Deployment FAILURE"
    echo "Check the deployment pipeline manually to find out the reason: ${deployment_pipeline_user_url}"
    exit 3
    ;;
  pending|running|preparing|waiting_for_resource|created) # nothing to do, wait
    ;;
  *)
    echo "We don't know this status. Probably the script is outdated."
    exit 42
    ;;
  esac
done
